<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('index');
});

Route::get('/detail', function () {
    return view('portfolio-details');
});


// Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');
Route::get('/movie/{id}', 'HomeController@movie')->middleware('auth')->name('movie');
Route::post('/movie/find', 'HomeController@find')->middleware('auth')->name('find');

Route::get('/backend', 'MovieController@index')->middleware('auth');
Route::get('/backend/create', 'MovieController@create')->middleware('auth');
Route::post('/backend/insert', 'MovieController@store')->middleware('auth');
Route::post('/backend/delete', 'MovieController@delete')->middleware('auth');
