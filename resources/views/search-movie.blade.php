@extends('layouts.app')

@section('content')
<style>
    section{
        padding: 120px 0 50px;
    }
    .movie{
        position: relative;
        background: #000;
    }
    .list_movie{
        cursor: pointer;
    }
</style>

<section id="movie" class="movie">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Movies</h2>
        <p>ค้นหา</p>
      </div>

      <div class="row">
        @if (count($movies) > 0)

            @foreach ($movies as $data)

            <div class="list_movie col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member" data-aos="fade-up" data-aos-delay="100">
                <div class="member-img">
                    <img src="{{ asset('cover')}}/{{ $data->img_cover }}" class="img-fluid" alt="" data-toggle="modal" data-target="#moive_detail_modal">
                    <div class="social">
                    <a href="/movie/{{ $data->id }}"><i class="icofont-ui-play"></i></a>
                    </div>
                </div>
                <div class="member-info" data-toggle="modal" data-target="#moive_detail_modal">
                    <h4>{{ $data->title }}</h4>
                    <span>{{ $data->description }}</span>
                </div>
                </div>
            </div>

            @endforeach

          @else
            <h1 style="text-align: center">ไม่พบที่ค้นหา</h1>
          @endif
      </div>

    </div>
  </section><!-- End movie Section -->

  <div class="modal fade" id="moive_detail_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>


@endsection
