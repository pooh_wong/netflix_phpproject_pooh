@if ($message = Session::get('success'))
<script>
    Swal.fire({
        title: '{{ $message }}',
        // text: 'Do you want to continue',
        icon: 'success',
        confirmButtonText: 'ตกลง'
    })
</script>
@endif


@if ($message = Session::get('error'))
<script>
    Swal.fire({
        title: '{{ $message }}',
        // text: 'Do you want to continue',
        icon: 'error',
        confirmButtonText: 'ตกลง'
    })
</script>
@endif


@if ($message = Session::get('warning'))
<script>
    Swal.fire({
        title: '{{ $message }}',
        // text: 'Do you want to continue',
        icon: 'warning',
        confirmButtonText: 'ตกลง'
    })
</script>
@endif


@if ($message = Session::get('info'))
<script>
    Swal.fire({
        title: '{{ $message }}',
        // text: 'Do you want to continue',
        icon: 'info',
        confirmButtonText: 'ตกลง'
    })
</script>
@endif


@if ($errors->any())
<script>
    Swal.fire({
        title: 'ไม่มีชื่อผู้ใช้งานนี้',
        // text: '',
        icon: 'error',
        confirmButtonText: 'ตกลง'
    })
</script>
@endif
