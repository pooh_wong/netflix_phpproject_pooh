<html>
    <body>
        <style>
            body{
                padding: 0;
                margin: 0;
            }
            iframe{
                width: 100%;
                height: 100%;
            }
            .btn_back{
                position: absolute;
                padding: 20px 40px;
                background-color: rgba(0, 0, 0, 0.5);
                font-size: 28px;
                color: #fff;
                text-decoration: none;
                transition: .5s;
            }
        </style>
        <a id="btnBack" href="/home" class="btn_back">
            กลับ
        </a>
        <div onmousemove="moveMouse(event)">
            {{-- <iframe src="https://www.youtube.com/embed/{{ $movie->link_video }}?autoplay=1&mute=0" frameborder="0" allowfullscreen></iframe> --}}
            <iframe src="https://www.youtube.com/embed/{{ $movie->link_video }}?autoplay=1" frameborder="0" allowfullscreen></iframe>
        </div>
    </body>
</html>
<script>
    var timeOut;
    function moveMouse(e) {
        clearTimeout(timeOut);
        var btnBack = document.getElementById("btnBack");
        btnBack.style.display = "block";

        timeOut = setTimeout(function(){
            btnBack.style.display = "none";
        }, 3000);
    }
    </script>
