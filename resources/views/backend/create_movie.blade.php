@extends('layouts.app-backend')

@section('content')
    <style>
        #img_preview{
            width: 300px;
        }
    </style>
    <section>
        <div class="container">
            <a href="/backend" class="btn btn-info">< กลับ</a>
            <hr>
            <h2>เพิ่มหนัง</h2>
            <hr>
            <form method="post" action="/backend/insert" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                  <label for="title" class="col-sm-2 col-form-label">ชื่อเรื่อง</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control-plaintext" id="title" name="title" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="description" class="col-sm-2 col-form-label">คำอธิบาย</label>
                  <div class="col-sm-10">
                    <textarea class="form-control-plaintext" name="description" id="description" cols="30" rows="3" required></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="link_video" class="col-sm-2 col-form-label">Link Video</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control-plaintext" id="link_video" name="link_video" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="img_cover" class="col-sm-2 col-form-label">เลือกรูป Cover</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control-plaintext" id="img_cover" name="img_cover" onchange="clickBtnFile(this)" required>
                  </div>
                </div>
                <div class="form-group row" id="wrap_preview">
                  <img src="" alt="" id="img_preview">
                </div>
                <div class="form-group row">
                  <label for="img_cover" class="col-sm-2 col-form-label"></label>
                  <div class="col-sm-10">
                    <button class="btn btn-primary">บันทึก</button>
                  </div>
                </div>
              </form>
        </div>
    </section>


<script>
    function clickBtnFile(input){
            if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#img_preview').attr('src', e.target.result)
                $('#wrap_preview').show('slow');
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
