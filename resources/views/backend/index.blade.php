@extends('layouts.app-backend')

@section('content')
    <style>
        .card-body img{
            max-width: 300px;
            max-height: 300px;
        }
        .card-body iframe{
            max-width: 300px;
            max-height: 300px;
        }
    </style>
    <section>
        <div class="container">
            <a href="/backend/create" class="btn btn-info">+ เพิ่มหนัง</a>
            <hr>
            <div id="accordion">
                @foreach ($movies as $data)

                <div class="card">
                  <div class="card-header" id="head{{$data->id}}">
                    <h5 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#col{{$data->id}}" aria-expanded="true" aria-controls="collapseOne">
                        {{$data->title}}
                      </button>
                    </h5>
                  </div>

                  <div id="col{{$data->id}}" class="collapse" aria-labelledby="head{{$data->id}}" data-parent="#accordion">
                    <div class="card-body">
                        <h3>{{$data->title}}</h3>
                        <div class="col-md-5">
                            <img src="{{ asset('cover')}}/{{$data->img_cover}}" alt="">
                            <p>{{$data->description}}</p>
                        </div>
                        <div class="col-md-5">
                            <iframe src="https://www.youtube.com/embed/{{$data->link_video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="col-md-2">
                            <form method="post" action="backend/delete">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <button class="btn btn-danger">ลบ</button>
                            </form>
                        </div>
                    </div>
                  </div>
                </div>

                @endforeach
              </div>
        </div>
    </section>
@endsection
