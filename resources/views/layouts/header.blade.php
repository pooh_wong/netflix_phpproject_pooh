<style>
    .search_box{
        width: 300px;
    }
</style>
<header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center justify-content-between">
        <a class="logo" href="/home"><img src="{{ asset('assets')}}/img/netflixlogo.png" alt=""></a>
        {{-- <h1 class="logo"><a href="/">Gp<span>.</span></a></h1> --}}
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo"><img src="{{ asset('assets')}}/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav class="nav-menu d-none d-lg-block" style="float: right">
            <ul>
                @guest

                <li><a href="/login">เข้าสู่ระบบ</a></li>
                <li><a href="/register">สมัครใช้งาน</a></li>
                @else
                    <li>
                        <form method="POST" action="/movie/find" class="form-inline d-flex justify-content-center md-form form-sm mt-0">
                            @csrf
                            <input id="search_box" class="form-control search_box" type="text" placeholder="ค้นหา"
                              aria-label="Search" name="search" style="display: none">
                            <button type="submit" id="click_search" style="display: none"></button>
                        </form>
                    </li>
                    <li>
                        <a href="#" id="icon_search"><i class="icofont-search-2"></i></a>
                    </li>

                    <li class="drop-down">
                        <a href="">บัญชี</a>
                        <ul>
                            <li><a href="#">โปรไฟล</a></li>
                            <li>
                                <a id="btnLogout" href="#">ออกจากระบบ</a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">
                                @csrf
                                <button id="logout" type="submit"></button>
                            </form>
                        </ul>
                    </li>
                @endguest

            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header>
<script src="{{ asset('assets')}}/vendor/jquery/jquery.min.js"></script>
<script>
    $( "#btnLogout" ).click(function() {
      $('#logout').click();
    });

    $( "#icon_search" ).click(function() {
      $('#search_box').show('slow');
    });

    $("#search_box").on('keyup', function (e) {
        if (e.keyCode === 13) {
            $('#click_search').click();
        }
    });
</script>
