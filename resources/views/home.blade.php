@extends('layouts.app')

@section('content')
<style>
    section{
        padding: 0;
    }
    .trailer_bg{
        height: 80vh;
    }
    .video_bg{
        position: absolute;
        top: -70px;
        width: 100%;
    }
    .video_bg iframe{
        width: 100vw;
        height: 120vh;
    }
    .detail_video{
        height: 80vh;

        background: -webkit-linear-gradient(13deg, rgba(0, 0, 0, 0.6) 0%, rgba(0, 0, 0, 0) 85%);
        background: -moz- oldlinear-gradient(13deg, rgba(0, 0, 0, 0.6) 0%, rgba(0, 0, 0, 0) 85%);
        background: -o-linear-gradient(13deg, rgba(0, 0, 0, 0.6) 0%, rgba(0, 0, 0, 0) 85%);
        background: linear-gradient(77deg, rgba(0, 0, 0, 0.6) 0%, rgba(0, 0, 0, 0) 85%);
        position: absolute;
        top: 0;
        left: 0;
        right: 26.09%;
        bottom: 0;
        opacity: 1;
        -webkit-transition: opacity 500ms;
        -o-transition: opacity 500ms;
        -moz-transition: opacity 500ms;
        transition: opacity 500ms;
        z-index: 5;
    }
    .gradians{
        background: -webkit-linear-gradient(rgba(255,0,0,0), rgba(0,0,0,1));
        background: -moz- oldlinear-gradient(rgba(255,0,0,0), rgba(0,0,0,1));
        background: -o-linear-gradient(rgba(255,0,0,0), rgba(0,0,0,1));
        background: linear-gradient(rgba(255,0,0,0), rgba(0,0,0,1));
        position: absolute;
        bottom: 15vh;
        height: 100px;
        width: 100%;
        z-index: 4;
    }
    .movie{
        position: relative;
        background: #000;
    }
    .list_movie{
        cursor: pointer;
    }
    @media screen and (max-width 992px){
        .gradians{
            display: none;
        }
    }
</style>
<section class="d-flex align-items justify-content trailer_bg">
    <div class="video_bg">
        <iframe src="https://www.youtube.com/embed/{{ $movie_recomend->link_video }}?autoplay=1&mute=1" frameborder="0" allowfullscreen></iframe>
        {{-- <iframe src="https://www.youtube.com/embed/HpZgwHU1GcI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
        {{-- <video autoplay="" playsinline="" muted="" loop="">
            <source src="https://assets.nflxext.com/ffe/siteui/acquisition/ourStory/fuji/desktop/video-devices.m4v" type="video/mp4">
        </video> --}}
    </div>
    <div data-aos="fade-up" class="detail_video">
        <div class="row justify-content" data-aos="fade-up" data-aos-delay="150" style="height: 100%">
        <div class="col-xl-6 col-lg-8" style="position: absolute;bottom: 0;padding: 50px;">
            <h1>{{ $movie_recomend->title }}</h1>
            <a href="/movie/{{ $movie_recomend->id }}" class="btn btn-primary btn-lg">ดูเลย</a>
        </div>
        </div>
    </div>
    <div class="gradians"></div>
</section>
<section id="movie" class="movie">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2 style="color: #fff">Movies</h2>
        <p style="color: #fff">หนังน่าดู</p>
      </div>

      <div class="row">

        @foreach ($movies as $data)

        <div class="list_movie col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="{{ asset('cover')}}/{{ $data->img_cover }}" class="img-fluid" alt="" data-toggle="modal" data-target="#moive_detail_modal">
                <div class="social">
                  <a href="/movie/{{ $data->id }}"><i class="icofont-ui-play"></i></a>
                </div>
              </div>
              <div class="member-info" data-toggle="modal" data-target="#moive_detail_modal">
                <h4 style="color: #fff">{{ $data->title }}</h4>
                <span>{{ $data->description }}</span>
              </div>
            </div>
          </div>

          @endforeach
      </div>

    </div>
  </section><!-- End movie Section -->

  <div class="modal fade" id="moive_detail_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

@endsection
