@extends('layouts.app')

@section('content')
<style type="text/css">
    .our-story-card-animation-container {
       margin: -5% -10% 0 0;
   }
   .our-story-card-img{
       z-index: 10;
        position: relative;
   }
   .our-story-card-animation {
        z-index: 5;
        position: absolute;
        width: 394px;
        top: 130px;
        left: 330px;
       -webkit-transform: translate(-50%, -50%);
       -moz-transform: translate(-50%, -50%);
       -ms-transform: translate(-50%, -50%);
       -o-transform: translate(-50%, -50%);
       transform: translate(-50%, -50%);
   }
   .our-story-card-animation video{
    width: 400px;
   }
</style>
  <section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container" data-aos="fade-up">

      <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
        <div class="col-xl-6 col-lg-8">
          <h1>ภาพยนตร์ รายการทีวี และความบันเทิงอีกมากมายแบบไม่จำกัด</h1>
          <h2>รับชมได้ทุกที่ ยกเลิกได้ทุกเมื่อ</h2>
        </div>
      </div>
    </div>
  </section>

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
            {{-- <img src="{{ asset('assets')}}/img/about.jpg" class="img-fluid" alt=""> --}}

            <div class="our-story-card-animation-container">
                <img alt="" class="our-story-card-img" src="https://assets.nflxext.com/ffe/siteui/acquisition/ourStory/fuji/desktop/device-pile.png" data-uia="our-story-card-img">
                <div class="our-story-card-animation">
                    <video class="our-story-card-video" autoplay="" playsinline="" muted="" loop="">
                        <source src="https://assets.nflxext.com/ffe/siteui/acquisition/ourStory/fuji/desktop/video-devices.m4v" type="video/mp4">
                    </video>
                    <div class="our-story-card-animation-text"></div></div></div>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right" data-aos-delay="100">
            <h3>รับชมได้ในทีวี</h3>
            <p class="font-italic">
                รับชมได้ในสมาร์ททีวี Playstation, Xbox, Chromecast, Apple TV เครื่องเล่น Blu-ray และอีกมากมาย
            </p>
            <h3>ดาวน์โหลดเนื้อหาไว้รับชมออฟไลน์</h3>
            <p class="font-italic">
                บันทึกเนื้อหาโปรดได้ง่ายๆ และมีความบันเทิงพร้อมให้รับชมอยู่เสมอ
            </p>
            <h3>รับชมได้ทุกที่</h3>
            <p class="font-italic">
                สตรีมภาพยนตร์และรายการทีวีได้ไม่จำกัดในโทรศัพท์ แท็บเล็ต แล็ปท็อป และทีวีโดยไม่ต้องจ่ายเพิ่ม
            </p>
            {{-- <ul>
              <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
            </ul>
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident
            </p> --}}
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <section id="cta" class="cta">
        <div class="container" data-aos="zoom-in">

          <div class="text-center">
            <h1>NETFLIX</h1>
          </div>

        </div>
      </section>
  </main>

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

@endsection
