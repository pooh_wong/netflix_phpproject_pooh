<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {

        if(Auth::user()->email == "admin@admin.com"){
            echo '<META HTTP-EQUIV="Refresh" CONTENT="0;URL=/backend">';
            return;
        }
        $movies = DB::table('movies')->orderByDesc('id')->get();

        $movie_recomend = new Movie;
        $random = rand(0,count($movies)-1);
        $movie_recomend->id = $movies[$random]->id;
        $movie_recomend->title = $movies[$random]->title;
        $movie_recomend->description = $movies[$random]->description;
        $movie_recomend->link_video = $movies[$random]->link_video;
        $movie_recomend->img_cover = $movies[$random]->img_cover;

        return view('home')
        ->with(compact('movies',$movies))
        ->with(compact('movie_recomend',$movie_recomend));
    }

    public function movie($id){
        $movie = DB::table('movies')->where('id',$id)->first();

        return view('movie')
        ->with(compact('movie',$movie));
    }

    public function find(Request $request){
        $movies = DB::table('movies')->where('title', 'like', '%' . $request->search . '%')->get();

        return view('search-movie')
        ->with(compact('movies',$movies));
    }
}
