<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = DB::table('movies')->orderByDesc('id')->get();

        return view('backend.index')
        ->with(compact('movies',$movies));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.create_movie');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file_img = $request->file('img_cover');

        $path = public_path().'/cover/';

        $fileName = $file_img->getClientOriginalName();
        $type = explode('.', $fileName);
        $indexTypeFile = count($type)-1;
        $typeImage = $type[$indexTypeFile];
        $newFileName = time().'.'.$typeImage;

        $link_video = $request->input('link_video');
        $newlink = substr($link_video,strpos($link_video,"watch?v=")+8);

        $movie = new Movie;
        $movie->title = $request->input('title');
        $movie->description = $request->input('description');
        $movie->link_video = $newlink;
        $movie->img_cover = $newFileName;

        if($movie->save()){
            $file_img->move($path, $newFileName);
        }
        back()->with('success','เพิ่มหนังเรียบร้อย');
        echo '<META HTTP-EQUIV="Refresh" CONTENT="0;URL=/backend">';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        DB::table('movies')->delete($request->id);
        back()->with('success','ลบหนังเรียบร้อย');
        echo '<META HTTP-EQUIV="Refresh" CONTENT="0;URL=/backend">';
    }
}
